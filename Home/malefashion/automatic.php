<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Male-Fashion | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>

        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="header__logo">
                        <a href="./index.html"><img src="img/logoo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <li><a href="./index.php">Home</a></li>
                            <li class="active"><a href="./automatic.php">Automatic</a></li>
                            <li><a href="./manual_operation.php">Manual Operation</a></li>
                            <li><a href="./contact.php">Contacts</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="header__nav__option">
                       <!-- <a href="#"><img src="img/icon/heart.png" alt="logout"></a>-->
                        <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#exampleModal">
                          Log out
                        </button>
                        
                        
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to log out?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-dialog modal-sm">
                            You can always sign back in at any time.
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary"><a href="/index.php">Log out</a></button>
                          </div>
                        </div>
                      </div>
                    </div>

                    </div>
                </div>
            </div>
            <div class="canvas__open"><i class="fa fa-bars"></i></div>
        </div>
        
    </header>
    <div class="container">

                  <div class="row">
                  <div class="col-6 col-md-4"><?=date('l,d M y, H:i')?></div>
                  </div                  </div>
 <div class="section-title">
            <section class="hero">
                        <br/><h4>กำหนดเงื่อนไขให้ระบบทำงานอย่างอัตโนมัติ</h4>
                                <span>Add scenc</span>

                                </div>
                                
                                
                                                     <!-- Button trigger modal -->
                           <div class="container">
                        <div class="col order-last">
                    <button  type="button" class="btn btn-warning" data-toggle="modal" data-target="#staticBackdrop">
                        <a><img src="img/icon/calendar.png"></a>
                      Add Scenc
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Add Scenc</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                          <h5>If</h5>
                          <p>This trigger</p>
                          
                           <form>
                                  <div class="form-group row">
                                    <label for="inputRepeat" class="col-sm-2 col-form-label">Repeat</label>
                                    <div class="col-sm-10">
                                     <select id="inputRepeat" class="form-control">
                                            <option selected>Choose..</option>
                                            <option value="Every Sunday">Every Sunday</option>
                                            <option value="Every Monday">Every Monday</option>
                                            <option value="Every Tuesday">Every Tuesday</option>
                                            <option value="Every Wednesday">Every Wednesday</option>
                                            <option value="Every Thursday">Every Thursday</option>
                                            <option value="Every Friday">Every Friday</option>
                                            <option value="Every Saturday">Every Saturday</option>

                                          </select>
                                        </div>
                            
                                    </div>
                                  
                                  
                                         
                     
                                  <div class="form-group row">
                                    <label for="inputTime" class="col-sm-2 col-form-label">Time</label>
                                    <div class="col-sm-10">
                                      <input type="time" class="form-control" id="appt"name="appt"
                                            min="00:00" max="23:00" required>
                                    </div>
                                  </div>
                                  
                                           
                                    <div class="form-group row">
                                    <label for="inputLight" class="col-sm-2 col-form-label">Light</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputLight"placeholder="Lux">
                                    </div>
                                  </div> 
                              
                                    <div class="form-group row">
                                    <label for="inputZone" class="col-sm-2 col-form-label">Zone</label>
                                    <div class="col-sm-10">
                                     <select id="inputZone" class="form-control">
                                            <option selected>Choose..</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                           

                                          </select>
                                        </div>
                            
                                    </div>
                                  
                                  
                           
                          <hr>
                          <h5>Then</h5>
                          <p>That actions</p>
                          <form>
                                      <div class="form-group row">
                                    <label for="inputZone" class="col-sm-2 col-form-label">Zone</label>
                                    <div class="col-sm-10">
                                     <select id="inputZone" class="form-control">
                                            <option selected>Choose..</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                          </select>
                                        </div>
                                    </div>
                                  
                                 <div class="form-group row">
                                    <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                     <select id="inputStatus" class="form-control">
                                            <option selected>Choose..</option>
                                            <option value="ON">ON</option>
                                            <option value="OFF">OFF</option>
                                          </select>
                                        </div>
                                    </div>
                                  
                                  
                                           
                                    <div class="form-group row">
                                    <label for="inputBrightness" class="col-sm-2 col-form-label">Brightness</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" id="inputBrightness"placeholder="Lux">
                                    </div>
                                  </div> 
                                  
                                  
                                   <div class="form-group row">
                                    <label for="inputEnergy Saving" class="col-sm-2 col-form-label">Energy Saving</label>
                                    <div class="col-sm-10">
                                     <select id="inputEnergy Saving" class="form-control">
                                            <option selected>Choose..</option>
                                            <option value="ON">ON</option>
                                            <option value="OFF">OFF</option>
                                          </select>
                                        </div>
                                    </div>
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  <hr>
                                  <div class="form-group row">
                                    <div class="col-sm-2">Checkbox</div>
                                    <div class="col-sm-10">
                                      <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="gridCheck1">
                                        <label class="form-check-label" for="gridCheck1">
                                          Check me out
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                         <div class="container">
                                            <div class="col order-last">
                                      <button type="submit" class="btn btn-primary btn-lg btn-block">Add</button>
                                    </div>
                                  </div>
                                </form>
                                    </div>
                                    </div>                

                          </div>
                        </div>
                      </div>
                    </div>
                    <br/>
                                                     
                            <table class="table table-hover">
                          <thead>
                            <tr>
                              <th scope="col">No.</th>
                              <th scope="col">If</th>
                              <th scope="col">Then</th>
                            <th scope="col">Status</th>
                              <th scope="col">Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                             <th scope="row">1</th>
                              <!--<td>Mark</td>
                              <td>Otto</td>
                              <td>@mdo</td>-->
                            </tr>
                            <tr>
                             <th scope="row">2</th>
                              <!--<td>Mark</td>
                              <td>Otto</td>
                              <td>@mdo</td>-->
                            </tr>
                            <tr>
                            <th scope="row">3</th>
                             <!--<td colspan="2">Larry the Bird</td>
                              <td>@twitter</td>-->
                            </tr>
                          </tbody>
                        </table>
            </div>
            </div>
    
    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    
     <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    
</body>

</html>
