<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Male-Fashion | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
        rel="stylesheet">

    <!-- Css Styles -->
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    
    
</head>

<body>
        <?php 
            // in case of POST retrieve from form.
            $filename = 'conf/lh_last_state.txt';
            $lines = file($filename, FILE_IGNORE_NEW_LINES);
            $street_state =  $lines[0];
            $park_state =  $lines[1];            
            $garden_state =  $lines[2];
            
            if(isset($_POST['street'])){
                $street_state = $_POST['street'];
                if ($street_state == 'on'){
                    $command = 'mosquitto_pub -t "LH" -m "LH01,setBrightness,255"';
                    system($command, $retval);
                }elseif ($street_state == 'save'){
                    
                }elseif ($street_state == 'off'){
                    $command = 'mosquitto_pub -t "LH" -m "LH01,setBrightness,0"';
                    system($command); 
                }                
                $lh_last_state = fopen($filename, "w") or die("Unable to open file!");
                $txt = $street_state."\n"
                .$park_state."\n"
                .$garden_state;
                fwrite($lh_last_state, $txt);
                fclose($lh_last_state);
            }elseif(isset($_POST['park'])){
                $park_state = $_POST['park'];
                if ($park_state == 'on'){
                    $command = 'mosquitto_pub -t "LH" -m "LH02,setBrightness,255"';
                    system($command, $retval);
                }elseif ($park_state == 'save'){
                    
                }elseif ($park_state == 'off'){
                    $command = 'mosquitto_pub -t "LH" -m "LH02,setBrightness,0"';
                    system($command); 
                }                
                $lh_last_state = fopen($filename, "w") or die("Unable to open file!");
                $txt = $street_state."\n"
                .$park_state."\n"
                .$garden_state;
                fwrite($lh_last_state, $txt);
                fclose($lh_last_state);
            }elseif(isset($_POST['garden'])){
                $garden_state = $_POST['garden'];
                if ($garden_state == 'on'){
                    $command = 'mosquitto_pub -t "LH" -m "LH03,setBrightness,255"';
                    system($command, $retval); 
                }elseif ($garden_state == 'save'){
                    
                }elseif ($garden_state == 'off'){
                    $command = 'mosquitto_pub -t "LH" -m "LH03,setBrightness,0"';
                    system($command); 
                }                
                $lh_last_state = fopen($filename, "w") or die("Unable to open file!");
                $txt = $street_state."\n"
                .$park_state."\n"
                .$garden_state;
                fwrite($lh_last_state, $txt);
                fclose($lh_last_state);
            }
            
        
        ?>
    
        
        
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="header__logo">
                        <a href="./index.html"><img src="img/logoo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <li><a href="./index.php">Home</a></li>
                            <li><a href="./automatic.php">Automatic</a></li>
                            <li class="active"><a href="./manual_operation.php">Manual Operation</a></li>
                            <li><a href="./contact.php">Contacts</a></li>
                        </ul>
                    </nav>
                </div>
                 <div class="col-lg-3 col-md-3">
                    <div class="header__nav__option">
                       <!-- <a href="#"><img src="img/icon/heart.png" alt="logout"></a>-->
                        <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#exampleModal">
                          Log out
                        </button>
                        
                        
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to log out?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-dialog modal-sm">
                            You can always sign back in at any time.
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary"><a href="/index.php">Log out</a></button>
                          </div>
                        </div>
                      </div>
                    </div>

                    </div>
                </div>
            </div>
            <div class="canvas__open"><i class="fa fa-bars"></i></div>
        </div>
    </header>
   

    </section>
     <div class="container">

                  <div class="row">
                  <div class="col-6 col-md-4"><?=date('l,d M y, H:i')?></div>
                  </div                  </div>
    <!-- Breadcrumb Section End -->
         
    <!-- Blog Section Begin -->
    <div class="section-title">
            <section class="hero">
                        <br/><h4>สถานที่ต่างๆภายในหมู่บ้าน</h4>
                                <span>Choose Mode</span>
                                </div>
                                
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic set-bg" data-setbg="img/s.png"></div>
                        <div class="blog__item__text">
                            <h5>Street lights</h5>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <form action="" method="post">
                                    <button type="submit" name="street" value="on" class="btn btn-secondary <?php if ($street_state == 'on'){echo 'btn btn-success';} ?>">On</button>
                                    <button type="submit" name="street" value="save" class="btn btn-secondary <?php if ($street_state == 'save'){echo 'btn btn-warning';} ?>">Save</button>
                                    <button type="submit" name="street" value="off" class="btn btn-secondary <?php if ($street_state == 'off'){echo 'btn btn-danger';} ?>">Off</button>
                                </form>
                            </div>
                        </div>
                   </div>
                </div>
                
                  
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic set-bg" data-setbg="img/park.png"></div>
                        <div class="blog__item__text">
                            <h5>Park lights</h5>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <form action="" method="post">
                                    <button type="submit" name="park" value="on" class="btn btn-secondary <?php if ($park_state == 'on'){echo 'btn btn-success';} ?>">On</button>
                                    <button type="submit" name="park" value="save" class="btn btn-secondary <?php if ($park_state == 'save'){echo 'btn btn-warning';} ?>">Save</button>
                                    <button type="submit" name="park" value="off" class="btn btn-secondary <?php if ($park_state == 'off'){echo 'btn btn-danger';} ?>">Off</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                        
                  
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic set-bg" data-setbg="img/park.png"></div>
                        <div class="blog__item__text">
                            <h5>All lighting control system</h5>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <form action="" method="post">
                                    <button type="submit" name="garden" value="on" class="btn btn-secondary <?php if ($garden_state == 'on'){echo 'btn btn-success';} ?>">On</button>
                                    <button type="submit" name="garden" value="save" class="btn btn-secondary <?php if ($garden_state == 'save'){echo 'btn btn-warning';} ?>">Save</button>
                                    <button type="submit" name="garden" value="off" class="btn btn-secondary <?php if ($garden_state == 'off'){echo 'btn btn-danger';} ?>">Off</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    
</body>

</html>
