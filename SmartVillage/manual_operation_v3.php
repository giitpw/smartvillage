<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Manual Operation</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    
    <?php 
            // in case of POST retrieve from form.
            $filename = 'conf/lh_last_state.txt';
            $lines = file($filename, FILE_IGNORE_NEW_LINES);
            $street_state =  $lines[0];
            $park_state =  $lines[1];            
            $garden_state =  $lines[2];
            
            if(isset($_POST['street'])){
                $street_state = $_POST['street'];
                if ($street_state == 'on'){
                    $command = 'mosquitto_pub -t "LH" -m "LH01,setBrightness,255"';
                    system($command, $retval);
                }elseif ($street_state == 'save'){
                    $command = 'mosquitto_pub -t "LH" -m "LH01,setBrightness,100"';
                    system($command, $retval);
                }elseif ($street_state == 'off'){
                    $command = 'mosquitto_pub -t "LH" -m "LH01,setBrightness,0"';
                    system($command); 
                }                
                $lh_last_state = fopen($filename, "w") or die("Unable to open file!");
                $txt = $street_state."\n"
                .$park_state."\n"
                .$garden_state;
                fwrite($lh_last_state, $txt);
                fclose($lh_last_state);
            }elseif(isset($_POST['park'])){
                $park_state = $_POST['park'];
                if ($park_state == 'on'){
                    $command = 'mosquitto_pub -t "LH" -m "LH02,setBrightness,255"';
                    system($command, $retval);
                }elseif ($park_state == 'save'){
                    $command = 'mosquitto_pub -t "LH" -m "LH02,setBrightness,100"';
                    system($command, $retval);
                }elseif ($park_state == 'off'){
                    $command = 'mosquitto_pub -t "LH" -m "LH02,setBrightness,0"';
                    system($command); 
                }                
                $lh_last_state = fopen($filename, "w") or die("Unable to open file!");
                $txt = $street_state."\n"
                .$park_state."\n"
                .$garden_state;
                fwrite($lh_last_state, $txt);
                fclose($lh_last_state);
            }elseif(isset($_POST['garden'])){
                $garden_state = $_POST['garden'];
                if ($garden_state == 'on'){
                    $command = 'mosquitto_pub -t "LH" -m "LH03,setBrightness,255"';
                    system($command, $retval);
                }elseif ($garden_state == 'save'){
                    $command = 'mosquitto_pub -t "LH" -m "LH03,setBrightness,100"';
                    system($command, $retval);
                }elseif ($garden_state == 'off'){
                    $command = 'mosquitto_pub -t "LH" -m "LH03,setBrightness,0"';
                    system($command); 
                }                
                $lh_last_state = fopen($filename, "w") or die("Unable to open file!");
                $txt = $street_state."\n"
                .$park_state."\n"
                .$garden_state;
                fwrite($lh_last_state, $txt);
                fclose($lh_last_state);
            }
        ?>
    
</head>

<body>
<?php include('header.php'); ?>
         
    <!-- Blog Section Begin -->
    <div class="section-title">
            <section class="hero">
                        <br/><h4>Choose Mode</h4>
                <span><?=date('l,d M y, H:i')?></span>

                                </div>
                                
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    
         

                    
                    <div class="blog__item">
                        <div class="blog__item__pic set-bg" data-setbg="img/Place/Street.png"></div>
                        <div class="blog__item__text">
                            <h5>Street lights</h5>
                                      <div class="btn-group" role="group" aria-label="Basic example">
                                <form action="" method="post">
                                    <button type="submit" name="street" value="on" class="btn btn-secondary <?php if ($street_state == 'on'){echo 'btn btn-success';} ?>">On</button>
                                    <button type="submit" name="street" value="save" class="btn btn-secondary <?php if ($street_state == 'save'){echo 'btn btn-warning';} ?>">Low</button>
                                    <button type="submit" name="street" value="off" class="btn btn-secondary <?php if ($street_state == 'off'){echo 'btn btn-danger';} ?>">Off</button>
                                </form>
                        </div>
                                    
                                
                        </div>
                   </div>
                </div>
                
                  
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic set-bg" data-setbg="img/Place/Park.png"></div>
                        <div class="blog__item__text">
                            <h5>Park lights</h5>
                            
                                     <div class="btn-group" role="group" aria-label="Basic example">
                                <form action="" method="post">
                                    <button type="submit" name="park" value="on" class="btn btn-secondary <?php if ($park_state == 'on'){echo 'btn btn-success';} ?>">On</button>
                                    <button type="submit" name="park" value="save" class="btn btn-secondary <?php if ($park_state == 'save'){echo 'btn btn-warning';} ?>">Low</button>
                                    <button type="submit" name="park" value="off" class="btn btn-secondary <?php if ($park_state == 'off'){echo 'btn btn-danger';} ?>">Off</button>
                                </form>
                                </div>
                                </div>
                            </div>
                        </div>
                        
                  
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic set-bg" data-setbg="img/Place/page.jpg"></div>
                        <div class="blog__item__text">
                            <h5>Light in font of the village</h5>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <form action="" method="post">
                                    <button type="submit" name="garden" value="on" class="btn btn-secondary <?php if ($garden_state == 'on'){echo 'btn btn-success';} ?>">On</button>
                                    <button type="submit" name="garden" value="save" class="btn btn-secondary <?php if ($garden_state == 'save'){echo 'btn btn-warning';} ?>">Low</button>
                                    <button type="submit" name="garden" value="off" class="btn btn-secondary <?php if ($garden_state == 'off'){echo 'btn btn-danger';} ?>">Off</button>
                                </form>
                        </div>
                    </div>
                </div>

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    
</body>

</html>

