<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link rel="stylesheet" href="style.css"/>
    <title>Patcharee Village</title>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
          rel="stylesheet">
    <!-- Css Styles -->

    <script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://raw.github.com/javadoug/jquery.drag-multiple/master/dist/jquery-ui.drag-multiple.min.js"></script>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
</head>

<body>
<?php include('header.php'); ?>
<div class="container">
    <svg width="0" height="0">
        <defs>
            <clipPath id="lhClip">
                <circle cx="500" cy="400" r="100" data-cid="1"/>
                <circle cx="500" cy="400" r="100" data-cid="2"/>
            </clipPath>
        </defs>
    </svg>

    <div class="row">
        <div class="col-md-10">
            <div class="parent" id="drag-zone">
                <img class="night" src="img/Place/Day.png"/>
                <img class="lh-1" src="img/Place/Day.png"/>
            </div>
        </div>
        <div class="col-md-2">
            <img class="light-item" id="l1" src="../images/lo2.png" width="50px" data-obj-id="1">
            <img class="light-item" id="l2" src="../images/lo2.png" width="50px" data-obj-id="2">
        </div>
    </div>

</div>

<script>

    var x;
    var y;
    $(document).ready(function () {

        getPoint();

        $(".light-item").draggable({
            multiple: true,
            drag: function (e, ui) {

                setPosition(e.clientX, e.clientY, $(this).data('obj-id'));
            }
        });

        $("#drag-zone").droppable({
            // tolerance: 'fit',
            drop: function (event, ui) {
                setPosition(event.clientX, event.clientY);
                var _obj_id = $(".light-item").data('obj-id');
                var _drag_id = ui.draggable.attr('data-obj-id');
                var _e_id = ui.draggable.prop('id');
                dropPoint(_drag_id, event.clientX, event.clientY, _e_id);

                console.log(_drag_id);

                var offset = $(this).offset();
                var xPos = offset.left;
                var yPos = offset.top;
            },
            over: function (event, ui) {
                setPosition(event.clientX, event.clientY);
            },
            out: function (event, ui) {
            }
        });
    })

    $(document).mousemove(function (e) {
        mouseX = e.pageX;
        mouseY = e.pageY;
        // console.log(`Cursor : ${mouseX} - ${mouseY}`)
        // setPosition(mouseX, mouseY);
    }).mouseover();


    function setPosition(xx, yy, id) {
        // console.log(id);
        $(`circle[data-cid=${id}]`).attr("cx", xx - 76);
        $(`circle[data-cid=${id}]`).attr("cy", yy - 130);
        // console.log(xx, yy);
        $(".lh-1").css(`-webkit-mask-image: radial-gradient(circle at ${xx}px ${yy}px, #000, transparent 10%);`);
    }

    function dropPoint(id, x, y, ele_id) {
        $.ajax({
            url: 'drop.php',
            type: 'post',
            dataType: 'json',
            data: {
                "id": id,
                "x": x,
                "y": y,
                "e_id": ele_id
            },
            success: function (e) {
                console.log(e);
            },
            error: function (e) {
                console.log(e);
                alert("ไม่สามารถวางจุดได้");
            }
        })
    }

    function getPoint() {
        $.ajax({
            url: 'get_point.php',
            type: 'get',
            dataType: 'json',
            success: function (r) {
                console.log(r);
                $.each(r, function (i, v) {
                    setPosition(v.position_x, v.position_y, i);
                    $(`#${v.element_id}`).css({'left': v.position_x - 1070, 'top': v.position_y - 160, 'position': 'relative'});

                    console.log(v.element_id);
                })
            },
            error: function (e) {

            }
        })
    }
</script>
</body>
</html>
